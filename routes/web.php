<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::pattern('student_no', 's[0-9]{10}');
Route::group(['prefix' => 'students'], function(){
	Route::get('{student_no}', [
		'as' => 'students',
		'uses' => 'StudentsController@getStudentData'
	]);

	Route::get('{student_no}/score/{subject?}', [
		'as' => 'students.score',
		'uses' => 'StudentsController@getStudentScore'
	])->where(['subject' => '(chinese|english|math)']);
});

Route::prefix('board')->group(function(){
    Route::get('/', 'BoardController@getIndex')->name('board.index');
    Route::get('/name', 'BoardController@getName')->name('board.name');
});

Route::group(['namespace' => 'Cool'], function(){
	Route::get('cool', 'TestController@index');
});
